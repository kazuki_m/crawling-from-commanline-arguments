from pymongo import MongoClient


class MongoDBClient:

    def __init__(self):
        self.client = MongoClient('localhost', 27017)

        self.db = self.client.crawling
        self.collection = self.db.url

    def insert_url(self, url):
        self.collection.insert_one({'url': url})
