import urllib
import urllib.robotparser

from crawl_website import CrawlWebsite


class ParseUrl:

    def check_can_crawl(self, raw_url):
        '''
        RobotFileParserにより、連携されたURLのrobots.txtを読み込む。クロール不可のサイトでなければクロールする。
        TODO: 正しくparseするためにhttp://hoge.comの形式に形成する必要有
        '''

        # TODO:メソッドが呼ばれる度にインスタンス化するのはメモリの無駄なのでinit時に１度インスタンス化するだけでよいが、parse_urlと再帰的にに呼び出す形となる
        self.i = CrawlWebsite()
        robots_txt = raw_url + '/robots.txt'

        robot_parser = urllib.robotparser.RobotFileParser()
        robot_parser.set_url(robots_txt)
        robot_parser.read()

        can_fetch = robot_parser.can_fetch("mybot", robots_txt)

        # ValueErrorは適切でない気がする
        if not can_fetch:
            raise ValueError("can not crawl this website")

        self.i.crawl(raw_url)
