import requests
import lxml.html

import time
from save_db import MongoDBClient


# 課題： URLという変数が多く自分でも処理が分かりにくいが、いい変数名が思い付かない
# 今回の目的では、基本的にURLをトップページまでの形に整形してから分析した方が良いかも
'''
2018/05/16 UnicodeDecodeError
https://www.google.co.jp/webhp? で発生(検索先がないときにRobotFileParserクラスのread()メソッドでDecode出来なくて落ちてるっぽい)
'''


class CrawlWebsite:

    def __init__(self):
        from parse_url import ParseUrl
        self.client = MongoDBClient()
        self.parse_client = ParseUrl()

    def crawl(self, url_parsed_by_robot):
        '''
        連携されたURLからaタグhref属性の絶対URLを取得する
        TODO: 連携されたURLからhref属性のURLをループで取得するが、現在のコードでは１件目に取得されたURLしか活用されていない。
        '''

        time.sleep(1)
        response = requests.get(url_parsed_by_robot)

        parsed_result = lxml.html.fromstring(response.content)
        parsed_result.make_links_absolute(response.url)

        for a in parsed_result.cssselect('a'):
            url_fetched_from_a_tag = a.get('href')
            self.push_url_to_db(url_parsed_by_robot,  url_fetched_from_a_tag)

    def push_url_to_db(self, url_parsed_by_robot, url_fetched_from_a_tag):
        '''
        自サイトのページ以外であれば、DBにインサートする。
        ＊ インサートする：http://hoge/fuga/hoge/hoge.com から http://hoge/fuga.com をクロール
        ＊ インサートしない： http://hoge/fuga.com から http://hoge/fuga/hoge/hoge.com をクロール
        ＊ オーソリティが同じURLはクローリング対象外としたい（amazon,楽天等のページから抜け出せなくなりそうなため） 
        '''
        if not url_parsed_by_robot in url_fetched_from_a_tag:
            print(url_fetched_from_a_tag)
            # TODO: RobotFileparserでnullの場合落ちるが、nullチェックしておく
            self.client.insert_url(url_fetched_from_a_tag)
            self.parse_client.check_can_crawl(url_fetched_from_a_tag)
